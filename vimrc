set nocompatible               " be iMproved
 filetype off                   " required!

 set rtp+=~/.vim/bundle/vundle/
 call vundle#rc()

 " let Vundle manage Vundle
 " required!
 Bundle 'gmarik/vundle'

 Bundle 'bitc/vim-bad-whitespace'
 Bundle 'kchmck/vim-coffee-script'
 Bundle 'kien/ctrlp.vim'
 Bundle 'scrooloose/nerdtree'
 Bundle 'scrooloose/syntastic'
 Bundle 'slim-template/vim-slim'
 Bundle 'tikhomirov/vim-glsl'
 Bundle 'tomtom/tcomment_vim'
 Bundle 'tpope/vim-rails'
 Bundle 'tpope/vim-haml'
 Bundle 'tpope/vim-rbenv'
 Bundle 'tpope/vim-fugitive'
 Bundle 'ruby.vim'
 Bundle 'Valloric/YouCompleteMe'
 Bundle 'wavded/vim-stylus'

syntax enable
filetype plugin indent on     " required!

colorscheme hybrid

" Tab indenting
set tabstop=2
set expandtab
set shiftwidth=2
set softtabstop=2
set ai " autoindent
set si " smart indent

" line numbering
set number

" line wrapping
set nowrap

" change leader key to comma
let mapleader = ","

" nerd tree shortcut
nmap <leader>t :NERDTreeToggle <cr>

" ctrlp
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

" move between tabs
map <leader>, :tabprevious <cr>
map <leader>. :tabnext <cr>
