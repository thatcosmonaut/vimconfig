#!/bin/bash
cd ~/.vim
git submodule init
git submodule update
git submodule foreach git checkout master
ln -s .vim/vimrc ../.vimrc
vim +BundleInstall! +qall
cd ~/.vim/bundle/YouCompleteMe
./install.sh --clang-completer
